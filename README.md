# Front-end framework v.1.0


## Build Setup  
  
```bash  
# install dependencies  
$ yarn install  
  
# serve with hot reload at localhost:3000  
$ yarn watch  
  
# build for production 
$ yarn build   
  
# generate static sprites file  
$ yarn sprites    
```  
  
For detailed explanation on how things work, check out [Nagiyev.pro](https://nagiyev.pro).